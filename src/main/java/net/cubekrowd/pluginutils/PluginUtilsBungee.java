/*

    pluginutils
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.pluginutils;

import com.google.common.collect.Iterables;
import com.google.common.io.ByteStreams;
import com.imaginarycode.minecraft.redisbungee.RedisBungee;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.Cleanup;
import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import net.cubekrowd.pluginutils.util.uuid.NameFetcher;
import net.cubekrowd.pluginutils.util.uuid.UUIDFetcher;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.user.User;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.chat.ComponentSerializer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import okhttp3.OkHttpClient;

public class PluginUtilsBungee {

    public final static UUID ZERO_UUID = new UUID(0, 0);

    @Getter private final Plugin plugin;

    public PluginUtilsBungee(@NonNull Plugin plugin) {
        this.plugin = plugin;

        OkHttpClient httpClient = new OkHttpClient();
//        Dispatcher dispatcher = new Dispatcher(getExecutorService());
//        httpClient.setDispatcher(dispatcher);
        NameFetcher.setHttpClient(httpClient);
        UUIDFetcher.setHttpClient(httpClient);

        if(checkRedisBungee()) {
            plugin.getLogger().info("RedisBungee detected. Using it for multi-bungee support.");
            RedisBungee.getApi().registerPubSubChannels(plugin.getDescription().getName() + "_PU");
            plugin.getProxy().getPluginManager().registerListener(plugin, new RedisListener(this));
        } else {
            plugin.getLogger().info("RedisBungee not detected. Skipping multi-bungee support...");
        }
    }

    /**
     * Check if RedisBungee is loaded.
     */
    public boolean checkRedisBungee() {
        return plugin.getProxy().getPluginManager().getPlugin("RedisBungee") != null;
    }

    /**
     * Get's this bungee server's ID.
     */
    public String getServerId() {
        if (checkRedisBungee()) {
            return RedisBungee.getApi().getServerId();
        } else {
            return "bungee";
        }
    }

    /**
     * Returns a Collection of all online usernames.
     */
    public Collection<String> getOnlineUsernames() {
        if (checkRedisBungee()) {
            // This returns null names : return RedisBungee.getApi().getHumanPlayersOnline();
            // Do the same code as getHumanPlayersOnline do, but force expensive lookup if not in redis (although which it always should be..)
            Set<String> names = new HashSet<>();
            for (UUID uuid : RedisBungee.getApi().getPlayersOnline()) {
                names.add(RedisBungee.getApi().getNameFromUuid(uuid, true));
            }
            return names;
        } else {
            return plugin.getProxy().getPlayers().stream().map(pi -> pi.getName()).collect(Collectors.toList());
        }
    }

    /**
     * Returns a Collection of all online UUID's.
     */
    public Collection<UUID> getPlayersOnline() {
        if (checkRedisBungee()) {
            return RedisBungee.getApi().getPlayersOnline();
        } else {
            return plugin.getProxy().getPlayers().stream().map(pi -> pi.getUniqueId()).collect(Collectors.toList());
        }
    }

    /**
     * Returns the UUID for a given name if they are online.
     */
    public UUID getUuidFromName(@NonNull String name) {
        return getUuidFromName(name, false);
    }

    /**
     * Returns the UUID for a given name if they are online, optionally looks it up.
     */
    @SneakyThrows
    public UUID getUuidFromName(@NonNull String name, boolean lookup) {
        if(checkRedisBungee()) {
            return RedisBungee.getApi().getUuidFromName(name, lookup);
        } else {
            ProxiedPlayer pp = plugin.getProxy().getPlayer(name);
            if(!lookup) {
                return pp == null ? null : pp.getUniqueId();
            } else {
                for (Map.Entry<String, UUID> entry : new UUIDFetcher(Arrays.asList(name)).call().entrySet()) {
                    if (entry.getKey().equalsIgnoreCase(name)) {
                        return entry.getValue();
                    }
                }
                return null;
            }
        }
    }

    /**
     * Returns the username for a given UUID if they are online.
     */
    public String getNameFromUuid(@NonNull UUID uuid) {
        return getNameFromUuid(uuid, false);
    }

    /**
     * Returns the username for a given UUID if they are online, optionally looks it up.
     */
    @SneakyThrows
    public String getNameFromUuid(@NonNull UUID uuid, boolean lookup) {
        if(checkRedisBungee()) {
            return RedisBungee.getApi().getNameFromUuid(uuid, lookup);
        } else {
            ProxiedPlayer pp = plugin.getProxy().getPlayer(uuid);
            if(!lookup) {
                return pp == null ? null : pp.getName();
            } else {
                return Iterables.getLast(NameFetcher.nameHistoryFromUuid(uuid), null);
            }
        }
    }

    /**
     * Returns the server a given player is on.
     */
    public ServerInfo getServerFor(@NonNull UUID uuid) {
        if(checkRedisBungee()) {
            return RedisBungee.getApi().getServerFor(uuid);
        } else {
            ProxiedPlayer pp = plugin.getProxy().getPlayer(uuid);
            return pp == null ? null : pp.getServer().getInfo();
        }
    }

    /**
     * Returns a Collection of all players on a server.
     */
    public Collection<UUID> getPlayersOnServer(@NonNull ServerInfo si) {
        if(checkRedisBungee()) {
            return RedisBungee.getApi().getPlayersOnServer(si.getName());
        } else {
            return si.getPlayers().stream().map(pi -> pi.getUniqueId()).collect(Collectors.toList());
        }
    }

    /**
     * Sends a message to a specific player.
     */
    public void sendMessagePlayer(@NonNull UUID uuid, @NonNull BaseComponent... message) {
        if(checkRedisBungee()) {
            RedisBungee.getApi().sendChannelMessage(plugin.getDescription().getName() + "_PU", "3#PM#" + uuid + "#" + ComponentSerializer.toString(message));
        } else {
            plugin.getProxy().getPlayer(uuid).sendMessage(message);
        }
    }

    /**
     * Sends a message to all players on a server.
     */
    public void sendMessageServer(@NonNull ServerInfo si, boolean filterMode, @NonNull BaseComponent... message) {
        if(checkRedisBungee()) {
            RedisBungee.getApi().sendChannelMessage(plugin.getDescription().getName() + "_PU", "4#SM#" + si.getName() + "#" + !filterMode + "#" + ComponentSerializer.toString(message));
        } else {
            si.getPlayers().stream().filter(pp -> pp.getChatMode() == ProxiedPlayer.ChatMode.SHOWN).forEach(pp -> pp.sendMessage(message));
        }
    }

    /**
     * Sends a message globally to everyone.
     */
    public void sendMessageGlobal(@NonNull BaseComponent... message) {
        if(checkRedisBungee()) {
            RedisBungee.getApi().sendChannelMessage(plugin.getDescription().getName() + "_PU", "2#GM#" + ComponentSerializer.toString(message));
        } else {
            plugin.getProxy().getPlayers().stream().forEach(pp -> pp.sendMessage(message));
        }
    }

    /**
     * Gets the UUID from a CommandSender.
     */
    public UUID getSenderUniqueId(@NonNull CommandSender sender) {
        return (sender instanceof ProxiedPlayer ? ((ProxiedPlayer) sender).getUniqueId() : new UUID(0, 0));
    }

    /**
     * Gets the server a CommandSender is on. Returned null if console.
     */
    public ServerInfo getSenderServer(@NonNull CommandSender sender) {
        return (sender instanceof ProxiedPlayer ? ((ProxiedPlayer) sender).getServer().getInfo() : null);
    }

    /**
     * Checks if the given UUID has a permission.
     */
    @SneakyThrows
    public boolean hasPermission(@NonNull UUID uuid, @NonNull String permission) {
        // Check if CONSOLE
        if(uuid.equals(ZERO_UUID)) {
            return true;
        }
        // Check if online locally
        ProxiedPlayer pp = plugin.getProxy().getPlayer(uuid);
        if(pp != null) {
            return pp.hasPermission(permission);
        }
        // Check LuckPerms
        LuckPerms api = LuckPermsProvider.get();
        User user = api.getUserManager().getUser(uuid);
        if(user == null) {
            user = api.getUserManager().loadUser(uuid).get();
        }
        return user.getCachedData().getPermissionData(api.getContextManager().getStaticQueryOptions()).getPermissionMap().getOrDefault(permission, false);
    }

    /**
     * Short-hand function for creating plugin folder, extracting default and loading custom configuration file.
     */
    @SneakyThrows
    public Configuration loadConfig(@NonNull String fileName) {
        if (!plugin.getDataFolder().exists()) {
            plugin.getDataFolder().mkdir();
        }
        File configFile = new File(plugin.getDataFolder(), fileName);
        if (!configFile.exists()) {
            configFile.createNewFile();
            @Cleanup InputStream is = plugin.getResourceAsStream(fileName);
            @Cleanup OutputStream os = new FileOutputStream(configFile);
            ByteStreams.copy(is, os);
        }
        return ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
    }
}
