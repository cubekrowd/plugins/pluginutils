/*

    pluginutils
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.pluginutils;

import java.util.*;
import java.io.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.plugin.*;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.chat.ComponentSerializer;
import net.md_5.bungee.event.EventHandler;
import com.imaginarycode.minecraft.redisbungee.RedisBungee;
import com.imaginarycode.minecraft.redisbungee.events.PubSubMessageEvent;

public class RedisListener implements Listener {

    private final PluginUtilsBungee pub;

    public RedisListener(PluginUtilsBungee pub) {
        this.pub = pub;
    }

    @EventHandler
    public void onPubSubMessage(PubSubMessageEvent e) {
        if (!e.getChannel().equals(pub.getPlugin().getDescription().getName() + "_PU")) {
            return;
        }

        // 3;<arg1>;<arg2>;>arg3>
        String[] msgb = e.getMessage().split("#", 2);
        String[] msga = msgb[1].split("#", Integer.parseInt(msgb[0]));

        CommandSender target;
        if(msga[0].equals("PM")) {
            UUID uuid = UUID.fromString(msga[1]);
            if(uuid.equals(new UUID(0, 0))) {
                target = pub.getPlugin().getProxy().getConsole();
            } else {
                target = pub.getPlugin().getProxy().getPlayer(uuid);
            }
            if (target != null) {
                target.sendMessage(ComponentSerializer.parse(msga[2]));
            }
        } else if(msga[0].equals("SM")) {
            Optional.ofNullable(pub.getPlugin().getProxy().getServerInfo(msga[1])).stream().forEach(si -> si.getPlayers().stream().filter(pp -> pp.getChatMode() == ProxiedPlayer.ChatMode.SHOWN || Boolean.valueOf(msga[2])).forEach(pp -> pp.sendMessage(ComponentSerializer.parse(msga[3]))));
        } else if(msga[0].equals("GM")) {
            pub.getPlugin().getProxy().getPlayers().stream().forEach(pp -> pp.sendMessage(ComponentSerializer.parse(msga[1])));
        }
    }

}
